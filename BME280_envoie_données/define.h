#ifndef __DEFINE_H__

#define __DEFINE_H__


typedef unsigned char byte;
typedef unsigned int word;

// Bits TWCR
#define TWINT           0x80
#define TWEA            0x40
#define TWSTA           0x20
#define TWSTO           0x10
#define TWEN            0x04
#define TWIE            0x01  

// Status TWSR
// Master transmit
#define MT_START        0x08
#define MT_REP_START    0x10
#define MT_SLA_ACK      0x18
#define MT_SLA_NACK     0x20
#define MT_DATA_ACK     0x28
#define MT_DATA_NACK    0x30
#define MT_LOST         0x38

// Master receive          
#define MR_LOST         0x38
#define MR_SLA_ACK      0x40
#define MR_SLA_NACK     0x48
#define MR_DATA_ACK     0x50
#define MR_DATA_NACK    0x58

#define TWI_OK          0
#define TWI_BUSY			0xFF

#endif