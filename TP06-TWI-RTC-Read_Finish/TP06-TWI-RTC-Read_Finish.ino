#ifndef __DEFINE_H__

#define __DEFINE_H__


typedef unsigned char byte;
typedef unsigned int word;

// Bits TWCR
#define TWINT           0x80
#define TWEA            0x40
#define TWSTA           0x20
#define TWSTO           0x10
#define TWEN            0x04
#define TWIE            0x01  

// Status TWSR
// Master transmit
#define MT_START        0x08
#define MT_REP_START    0x10
#define MT_SLA_ACK      0x18
#define MT_SLA_NACK     0x20
#define MT_DATA_ACK     0x28
#define MT_DATA_NACK    0x30
#define MT_LOST         0x38

// Master receive          
#define MR_LOST         0x38
#define MR_SLA_ACK      0x40
#define MR_SLA_NACK     0x48
#define MR_DATA_ACK     0x50
#define MR_DATA_NACK    0x58

#define TWI_OK          0
#define TWI_BUSY			0xFF

#endif


unsigned short digT1;
short digT2;
short digT3;

unsigned short dig_H1;
short dig_H2;
short dig_H3;
short dig_H4;
short dig_H5;
short dig_H6;

byte i2c_sla_addr;
// WARNING: i2c_status absolument volatile, car est modifiée dans la tâche de fond (twi_write ou read) et dans l'IT
// si pas volatile, ça ne marche pas (la var n'est pas correctement modifiée et on est dans une boucle infinie dans twi_write ou read
volatile byte i2c_status;     
byte i2c_len;
byte i2c_buf[32];

/*-------------------------------------------------------------------------
  I Fonction     : ISR(TWI_vect)                                          I
  -------------------------------------------------------------------------
  I Action       : Routine d'IT TWI                                       I
  I Param entrée : Rien                                                   I
  I Retour       : Aucun                                                  I
  I                                                                       I
  ------------------------------------------------------------------------*/

ISR(TWI_vect)
{
	byte status;
	static byte index;
	
	status = TWSR & 0xF8;					// Lecture status TWI
	switch (status)
	{
		// Condition de START ou START REPEATED correctement générée => on envoie l'@ de l'esclave 
		// (peu importe W/R, on s'en fout c'est pareil!
		case MT_START:
		case MT_REP_START:
			TWDR = i2c_sla_addr;            // @ esclave (@ du chip I2C)
			TWCR = TWINT | TWEN | TWIE;     // Descend bit TWINT et le TWSTA et envoie adresse
			break;
			
		// Mode écriture (W), @ esclave envoyée et ACK bien reçu => le chip est là, il a répondu, on envoie première donnée
		case MT_SLA_ACK:
			index = 0;
			TWDR = i2c_buf[0];
			TWCR = TWINT | TWEN | TWIE;     // Descend bit TWINT et envoie data
			break;
			
		// Mode écriture (W), donnée envoyée et acquitée => on envoie donnée suivante s'il en reste, ou génère STOP et on indique fin avec i2c_status = TWI_OK;
		case MT_DATA_ACK:
			index++;
			if (index < i2c_len)
			{
				TWDR = i2c_buf[index];
				TWCR = TWINT | TWEN | TWIE;     // Descend bit TWINT et envoie data
			}
			else
			{
				TWCR = TWINT | TWSTO | TWEN | TWIE;   // Envoie STOP condition 
				delayMicroseconds(10);                // Impératif le délai pour que la condition STOP se fasse
				TWCR &= ~TWSTO;                       // RAZ du STOP (obligatoire, voir datasheet) 
				TWCR |= TWINT;
				i2c_status = TWI_OK;
			}
			break;
			
		// Mode lecture (R), @ esclave envoyée et ACK bien reçu => le chip est là, il a répondu, on va lire première donnée
		case MR_SLA_ACK:
 			index = 0;
			TWCR = TWINT | TWEA | TWEN | TWIE;           // Descend bit TWINT monte TWEA pour générer ACK
			break;
			
		// Mode lecture (R), donnée bien reçue => on lit donnée suivante s'il en reste à lire, ou on indique fin avec i2c_status = TWI_OK;
		// pour la dernière donnée, on ne lèvera pas TWEA pour ne pas acquiter et du coup, le status à la prochaine IT sera MR_DATA_NACK
		// et va générer la condition de STOP plus bas
		case MR_DATA_ACK:
			i2c_buf[index] = TWDR;						// Stocke l'octet reçu
			if (index < i2c_len - 1)
			{
				// Il reste encore au moins un donnée à recevoir
				TWCR = TWINT | TWEA | TWEN | TWIE;      // Descend bit TWINT monte TWEA pour générer ACK
			}
			else
			{
				// C'est la dernière donnée
				TWCR = TWINT | TWEN | TWIE;           	// Descend bit TWINT et lit data: TWEA n'est pas positionné pour ne pas générer ACK
				i2c_status = TWI_OK;
			}
			index++;
			break;

		// MR_DATA_NACK correspond à la réception de la dernière donnée => va générer la condition de STOP
		case MR_DATA_NACK:
		// Dans tous ces cas, quelque chose s'est mal passé, on tente simplement une condition de STOP et on enregistre le status dans i2c_status
		case MT_LOST:
		case MT_SLA_NACK:
		case MT_DATA_NACK:
		case MR_SLA_NACK:
		default:
			i2c_status = status;
			TWCR = TWINT | TWSTO | TWEN | TWIE;   // Envoie STOP condition 
			delayMicroseconds(10);                // Impératif le délai pour que la condition STOP se fasse
			TWCR &= ~TWSTO;                       // RAZ du STOP (obligatoire, voir datasheet) 
			TWCR |= TWINT;
			break;
	}
	
	TWCR |= TWINT;							// Clear obligatoire du flag d'IT qui n'est pas RAZ par le hardware
}


/*-------------------------------------------------------------------------
  I Fonction     : twi_init                                               I
  -------------------------------------------------------------------------
  I Action       : Inititalisation TWI                                    I
  I Param entrée : Rien                                                   I
  I Retour       : Aucun                                                  I
  I                                                                       I
  ------------------------------------------------------------------------*/

void twi_init(void)
{
   // 2 Wire Bus initialization
   // Generate Acknowledge Pulse: On
   // 2 Wire Bus Slave Address: 0h
   // General Call Recognition: Off
   // Bit Rate: 100,000 kHz
   TWSR = 0x00;
   TWBR = 0x48;                 // 400kHz
   TWAR = 0x00;
   TWCR = TWEA | TWEN | TWIE;
}        


/*-------------------------------------------------------------------------
  I Fonction     : twi_write                                              I
  -------------------------------------------------------------------------
  I Action       : Envoi de len octets vers un périph I2C                 I
  I Param entrée : sla_addr: @ slave, len: bnre d'octets, buf:  les data  I
  I Retour       : Résultat de la transaction                             I
  I Commentaires :                                                        I
  ------------------------------------------------------------------------*/

byte twi_write(byte sla_addr, byte len, byte *buf)
{
	byte status;
	byte i;

	memcpy(i2c_buf, buf, len);
	i2c_sla_addr = sla_addr & 0xFE;
	i2c_len = len;
	i2c_status = TWI_BUSY;
	TWCR = TWINT | TWSTA | TWEN | TWIE;           // Envoie START
	while (i2c_status == TWI_BUSY);               // et attend que l'IT ait fini son taf et change le i2c_status
	
	return (i2c_status);
}
   



/*-------------------------------------------------------------------------
  I Fonction     : twi_read                                               I
  -------------------------------------------------------------------------
  I Action       : Lit len octets dans un périph I2C                      I
  I Param entrée : sla_addr: @ slave, len: bnre d'octets, buf:  les data  I
  I Retour       : Résultat de la transaction                             I
  I Commentaires :                                                        I
  ------------------------------------------------------------------------*/

byte twi_read(byte sla_addr, byte len, byte *buf)
{
	byte status;
	byte i;

	i2c_sla_addr = sla_addr | 0x01;
	i2c_len = len;
	i2c_status = TWI_BUSY;
	TWCR = TWINT | TWSTA | TWEN | TWIE;           // Envoie START
	while (i2c_status == TWI_BUSY);               // et attend que l'IT ait fini son taf et change le i2c_status
	
	if (i2c_status == TWI_OK)                     // si tout est ok, alors on recopie dans le buffer de sortie    
	   memcpy(buf, i2c_buf, len);
 	
	return (i2c_status);
}
   

/*-------------------------------------------------------------------------
  I Fonction     : BME380_compensate_T                                    I
  -------------------------------------------------------------------------
  I Action       : Compense la température avec les coefs                 I
  I Param entrée : adc_T: résultat de la mesure (voir datasheet BMS280)   I
  I Retour       : Température                                            I
  I Commentaires :                                                        I
  ------------------------------------------------------------------------*/

long BME380_compensate_T(long adc_T)
{
  long t_fine, var1, var2, T;
  
  var1 = ((((adc_T>>3) - ((long)digT1<<1))) * ((long)digT2)) >> 11;
  var2 = (((((adc_T>>4) - ((long)digT1)) * ((adc_T>>4) - ((long)digT1))) >> 12) * ((long)digT3)) >> 14;
  t_fine = var1 + var2;
  T = (t_fine * 5 + 128) >> 8;

  return T;
}


// Returns humidity in %rH as as double. Output value of "46.332" represents 46.332 xm
// Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
// Output value of “47445” represents 47445/1024 = 46.333 %RH
unsigned long bme280_compensate_H(long adc_H)
{
  long v_x1_u32r;
  long t_fine;
  v_x1_u32r = (t_fine - ((long)76800));
  v_x1_u32r = (((((adc_H << 14)-(((long)dig_H4) << 20)-(((long)dig_H5) * v_x1_u32r)) +
    ((long)16384)) >> 15) * (((((((v_x1_u32r * ((long)dig_H6)) >> 10) * (((v_x1_u32r *
    ((long)dig_H3)) >> 11) + ((long)32768))) >> 10) + ((long)2097152)) *
    ((long)dig_H2) + 8192) >> 14));
  v_x1_u32r = (v_x1_u32r-(((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((long)dig_H1)) >> 4));
  v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
  v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
return (unsigned long)(v_x1_u32r>>12);
}


/*-------------------------------------------------------------------------
  I Fonction     : setup                                                  I
  -------------------------------------------------------------------------
  I Action       : -                                                      I
  ------------------------------------------------------------------------*/
void setup() 
{
  int i;
  byte status;
  byte buf[16];
 
 // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("\r\n============== setup ===============");
 
  twi_init();

  // Config reg ctrl_meas (0xF2) = 0x01
  buf[0] = 0xF2;
  buf[1] = 0x01;
  status = twi_write(0xEC, 2, buf);
  Serial.print("Baromètre 1: "); Serial.println(status, HEX);

  // Config reg ctrl_meas (0xF4) = 0x23
  buf[0] = 0xF4;
  buf[1] = 0x23;
  status = twi_write(0xEC, 2, buf);
  Serial.print("Baromètre 2: "); Serial.println(status, HEX);


 
  // Config reg ctrl_meas (0x00) = 0x80
  buf[0] = 0x00;
  buf[1] = 0x00;
  status = twi_write(0xD0, 2, buf);
  Serial.print("Horloge 1: "); Serial.println(status, HEX);

  // Init du pointeur interne du BME à 0x88
  buf[0] = 0x88;
  status = twi_write(0xEC, 1, buf);
  Serial.print("2: "); Serial.println(status, HEX);

  // Lecture des 6 octets de calib => buf[0..5]
  status = twi_read(0xED, 6, buf);
  Serial.print("3: "); Serial.println(status, HEX);

  Serial.print("Calib bytes = ");
  for (i = 0; i < 6; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.println();

  digT1 = (unsigned short)buf[0] | ((unsigned short)buf[1] << 8);
  digT2 = (short)buf[2] | ((short)buf[3] << 8);
  digT3 = (short)buf[4] | ((short)buf[5] << 8);

  /*************************************** Himidity setting *********************/
  buf[0] = 0xA1;
  status = twi_write(0xEC, 1, buf);
  Serial.print("0xA1: "); Serial.println(status, HEX);

  // Lecture des 1 octets de calib => buf[0..1]
  status = twi_read(0xED, 1, buf);
  Serial.print("0xA1: "); Serial.println(status, HEX);

  Serial.print("Calib bytes = ");
  for (i = 0; i < 1; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.println();

  dig_H1 = (unsigned short)buf[0];

  buf[0] = 0xE1;
  status = twi_write(0xEC, 1, buf);
  Serial.print("0xE1: "); Serial.println(status, HEX);

  // Lecture des 1 octets de calib => buf[0..1]
  status = twi_read(0xED, 2, buf);
  Serial.print("0xE1: "); Serial.println(status, HEX);

  Serial.print("Calib bytes = ");
  for (i = 0; i < 2; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.println();

  dig_H2 = (short)buf[0] | ((short)buf[1] << 8) ;

  buf[0] = 0xE3;
  status = twi_write(0xEC, 1, buf);
  Serial.print("0xE3: "); Serial.println(status, HEX);

  // Lecture des 1 octets de calib => buf[0..5]
  status = twi_read(0xED, 5, buf);
  Serial.print("0xE3: "); Serial.println(status, HEX);

  Serial.print("Calib bytes = ");
  for (i = 0; i < 5; i++) 
  { 
    Serial.print(i);
     Serial.print(':');
    Serial.print(buf[i], BIN);
    Serial.print(' ');
  }
  Serial.println();
  
  dig_H3 = (unsigned short)buf[0];
  dig_H4 = ((short)buf[1] << 4) | ((short)buf[2] & 0b00001111);
  dig_H5 = (((short)buf[2] & 0b11110000) >> 4) | ((short)buf[3] << 4);
  dig_H6 = (short)buf[4];

  Serial.print("digT1="); Serial.print(digT1, HEX);
  Serial.print(" - digT2="); Serial.print(digT2, HEX);
  Serial.print(" - digT3="); Serial.println(digT3, HEX);

  Serial.print("dig_H1="); Serial.print(dig_H1, HEX);
  Serial.print(" - dig_H2="); Serial.print(dig_H2, HEX);
  Serial.print(" - dig_H3="); Serial.println(dig_H3, HEX);
  Serial.print(" - dig_H4="); Serial.print(dig_H4, HEX);
  Serial.print(" - dig_H5="); Serial.print(dig_H5, HEX);
  Serial.print(" - dig_H6="); Serial.println(dig_H6, HEX);
  /*************************************** END Himidity setting *********************/
  Serial.println("============== end setup ===============\r\n");
}


/*-------------------------------------------------------------------------
  I Fonction     : loop                                                   I
  -------------------------------------------------------------------------
  I Action       : -                                                      I
  ------------------------------------------------------------------------*/
void loop()
{
  int i;
  long temp;
  long humi;
  float t;
  static int n = 1;						// Bin oui, la boucle principale est appelée par une autre fonction (le main sans doute) et n'est donc pas statique...
  byte buf[16];
  
  // Init du pointeur interne du BME à 0xFA
  buf[0] = 0xFA;
  twi_write(0xEC, 1, buf);

  // Lecture des 3 octets de temp => buf[0..2]
  twi_read(0xED, 3, buf);

  Serial.print(n);
  Serial.print(": Temp bytes = ");
  n++;
  for (i = 0; i < 3; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.print(" => ");

  temp = ((long)buf[2] >> 4) | ((long)buf[1] << 4) | ((long)buf[0] << 12);
  t = (float)BME380_compensate_T(temp) / 100.0;
  Serial.print("T = ");
  Serial.print(t);
  Serial.println(" °C");
  
   // Init du pointeur interne de l'horloge à 0x00
  buf[0] = 0x00;
  twi_write(0xD0, 1, buf);

  // Lecture des 3 octets de temp => buf[0..2]
  twi_read(0xD0, 3, buf);

  for (i = 0; i < 3; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.print(" => ");
  Serial.print("Time = ");
  Serial.print(buf[2] , HEX);
  Serial.print(" h ");
  Serial.print(buf[1] , HEX);
  Serial.print(" min ");
  Serial.print(buf[0] , HEX);
  Serial.println(" sec");

   // Init du pointeur interne du BME à 0xF
  buf[0] = 0xFD;
  twi_write(0xEC, 1, buf);

  // Lecture des 3 octets de temp => buf[0..2]
  twi_read(0xED, 2, buf);

  Serial.print(n);
  Serial.print(": Temp bytes = ");
  n++;
  for (i = 0; i < 3; i++) 
  { 
    Serial.print(buf[i], HEX);
    Serial.print(' ');
  }
  Serial.print(" => ");

  humi = ((long)buf[1]) | ((long)buf[0] << 8);
  t = (float)bme280_compensate_H(humi) / 1024;
  Serial.print("Humidity = ");
  Serial.print(t);
  Serial.println(" %");
  delay(5000);
}
